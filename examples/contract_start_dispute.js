const util = require("util");
const inspect = (o) => util.inspect(o, false, null, true);

const hodl = require("./config");

const readline = require("readline");
function askQuestion(query) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise(resolve => rl.question(query, ans => {
    rl.close();
    resolve(ans);
  }))
};

async function run() {
  const contractId = await askQuestion(
    "Enter contract ID, where status is 'paid' and:\n" +
      "1) you are the seller, or\n" +
      "2) you are the buyer and payment window has expired\n"
  );
  console.log("Contract ID:", contractId);

  console.log();
  console.log("Starting dispute\n");
  result = hodl.startDispute(contractId);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");
};

run();
