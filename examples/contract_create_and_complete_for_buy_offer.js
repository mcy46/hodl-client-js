const util = require("util");
const inspect = (o) => util.inspect(o, false, null, true);

const hodl = require("./config");
const offerId = hodl.exampleBuyOfferId;
const paymentPassword = hodl.paymentPassword;

const keypress = async () => {
  process.stdin.setRawMode(true);
  return new Promise(resolve => process.stdin.once("data", () => {
    process.stdin.setRawMode(false);
    resolve();
  }))
}

async function run() {
  console.log("Getting encrypted seed\n");
  let result = hodl.getMyself();
  result = await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );

  const encryptedSeed = result.user.encrypted_seed;
  console.log("Encrypted seed:", encryptedSeed);
  const decryptedSeed = hodl.crypto.decryptSeed(encryptedSeed, paymentPassword);
  console.log("Decrypted seed:", decryptedSeed);
  console.log("\n");

  console.log("Getting counterparty's offer\n");
  result = hodl.getOffer(offerId);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );
  console.log("\n");

  const offer = result.offer;
  const offerVersion = offer.version;
  const pmId = offer.payment_methods[0].id;

  console.log("Creating payment method instruction for this offer\n");
  result = hodl.createPaymentMethodInstruction({payment_method_instruction: {
    payment_method_id: pmId,
    name: `Test instruction (${Date.now()})`,
    details: `Send money to account #123`
  }});
  result = await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );
  console.log("\n");

  const pmiId = result.payment_method_instruction.id;
  const pmiVersion = result.payment_method_instruction.version;

  console.log("Creating contract\n");
  result = hodl.createContract({contract: {
    offer_id: offerId,
    offer_version: offerVersion,
    payment_method_instruction_id: pmiId,
    payment_method_instruction_version: pmiVersion,
    value: 11
  }});
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  let contract = result.contract;
  console.log("\n");

  while (contract.escrow.address === null) {
    console.log(`Waiting for the counterparty to generate escrow (press any key to check)`);
    await keypress();

    console.log("Checking contract");
    result = await hodl.getContract(contract.id).then(
      function(success) { console.log(success); return success; },
      function(error) { console.log("error:"); console.log(error) }
    );

    contract = result.contract;
  }

  console.log("Veryfing escrow address\n");
  console.log("Public key index:", contract.escrow.index);
  const pubkey = hodl.crypto.pubkeyFromDecryptedSeed(decryptedSeed, contract.escrow.index);
  console.log("Public key:", pubkey);
  if (hodl.crypto.verifyContract(contract, pubkey) === true) {
    console.log("Success");
  } else {
    console.log("Failure");
    return;
  }
  console.log("\n");

  console.log("Confirming contract\n");
  result = hodl.confirmContract(contract.id);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");

  while (contract.status === "pending") {
    console.log(`Waiting for the buyer to confirm contract (press any key to check)`);
    await keypress();

    console.log("Checking contract status");
    result = await hodl.getContract(contract.id).then(
      function(success) { console.log(success); return success; },
      function(error) { console.log("error:"); console.log(error) }
    );

    contract = result.contract;
  }

  while (contract.status === "depositing") {
    console.log(`Waiting for you to deposit ${contract.volume} BTC now to ${contract.escrow.address} (press any key)`);
    await keypress();

    console.log("Checking contract status");
    result = await hodl.getContract(contract.id).then(
      function(success) { console.log(success); return success; },
      function(error) { console.log("error:"); console.log(error) }
    );

    contract = result.contract;
  }

  while (contract.status === "in_progress") {
    console.log(`Waiting for contract to be marked as paid by buyer (press any key)`);
    await keypress();

    console.log("Checking contract status");
    result = await hodl.getContract(contract.id).then(
      function(success) { console.log(success); return success; },
      function(error) { console.log("error:"); console.log(error) }
    );

    contract = result.contract;
  }

  console.log("Downloading release transaction\n");
  result = hodl.showReleaseTransaction(contract.id);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  const releaseTransactionData = result.transaction;
  console.log("\n");

  console.log("Signing release transaction on the client\n");
  const privkey = hodl.crypto.privkeyFromDecryptedSeed(decryptedSeed, contract.escrow.index);
  console.log("Privkey:", privkey);
  const signedTransactionHex = hodl.crypto.signTransaction(releaseTransactionData, privkey);
  console.log("\n");

  console.log("Sending signed transaction to the server\n");
  result = hodl.signReleaseTransaction(contract.id, signedTransactionHex);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");

  console.log("Done here. Press Ctrl+C to exit.");
}

run();
