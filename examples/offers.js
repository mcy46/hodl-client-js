const util = require("util");
const inspect = (o) => util.inspect(o, false, null, true);

const hodl = require("./config");

async function run () {
  console.log("Creating offer\n");
  let result = hodl.createOffer({offer: {
    side: "buy",
    currency_code: "USD",
    balance: 10000,
    min_amount: 50,
    max_amount: 0,
    payment_method_ids: [1],
    price_source: "fixed_value",
    price: 700
  }});
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  const offerId = result.offer.id;
  console.log(`Created offer with ID = ${offerId}`);
  console.log("\n");

  console.log("Getting offer\n");
  result = hodl.getOffer(offerId);
  await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );
  console.log("\n");

  console.log("Listing/searching offers\n");
  result = hodl.listOffers({filters: {side: "sell", currency_code: "USD", volume: 9999}});
  await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");

  console.log("Fetching newly created offers\n");
  result = hodl.fetchNewOffers({filters: {side: "sell", currency_code: "USD", volume: 9999}});
  await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");

  console.log("Updating offer\n");
  result = hodl.updateOffer(offerId, {offer: {
    price_source: "fixed_value",
    currency_code: "USD",
    price: 710
  }});
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");

  console.log("Deleting offer\n");
  result = hodl.deleteOffer(offerId);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");
};

run();
