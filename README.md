# HodlHodl API javascript client

See [API documentation](https://hodlhodl.com/api/docs) on HodlHodl website.

# Contract operations workflow

Basic workflow is as follows:

1. Buyer or seller sends "Creating contract" request to the server
1. Initiator sends "Getting contract" request once every 1-3 minutes until `contract.escrow.address` is not `null` (thus, waiting for offer's creator to confirm his payment password in case he uses the website)
1. Each party verifies the escrow address locally
1. Each party sends "Confirming contract's escrow validity" request to the server
1. Seller deposits cryptocurrency to escrow address
1. Buyer checks contract status with "Getting contract" request to the server once every 1-5 minutes until contract status is "in_progress"
1. Buyer verifies locally that correct amount of cryptocurrency has been deposited to escrow address and correct number of blockchain confirmations has been received
1. Buyer sends fiat payment to the seller
1. Buyer sends "Marking contract as paid" request to the server
1. Seller verifies locally that he actually has received fiat payment
1. Seller fetches pre-signed by the server release transaction with "Show release transaction of contract" request
1. Seller verifies and signs the release transaction locally
1. Seller sends the signed release transaction to the server with "Signing release transaction of contract"

Note: both buyer and seller could check contract status (including information on deposited cryptocurrency) at any time with "[Getting contract](#contracts-creating-contract)" request.

See also API documentation.

# Examples

Open `examples/config.js` and fill in your data:

* API host (e.g. `https://hhtestnet.com`)
* API key
* Payment password
* Signature key (for creating contracts with release address specified)
* Arbitrary sell offer ID, which belongs to another user (required only for contract examples)
* Arbitrary buy offer ID, which belongs to another user (required only for contract examples)

After you've done `npm install` run specific examples with `node examples/<filename>.js`.

See `/examples` directory for the list of examples:

* `offers.js` - offer operations
* `payment_method_instructions.js` - payment method instructions operations
* `contract_create_and_cancel.js` - creating contract with custom release_address and cancelling it
* `contract_create_and_complete_for_buy_offer.js` - create and copmlete contract from seller's point of view
* `contract_create_and_complete_for_sell_offer.js` - create and complete contract from the buyer's point of view
* `contract_refund.js` - refund arbitrary contract (where you are seller and have won the dispute)

Note: currenly contract examples doesn't implement verification of the release transaction.
